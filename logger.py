import logging
import os
from datetime import date

log_dir_path = '/var/fantasy/'
if not os.path.exists(log_dir_path):
    os.makedirs(log_dir_path)

class CrawlerLogger:
    file_name = "crawler"
    fmt ='%(levelname)s %(asctime)s %(name)s: %(message)s'
    formatter = logging.Formatter(fmt = fmt, datefmt = '%m/%d/%Y %H:%M:%S')

    @classmethod
    def getLogger(cls,name,level=logging.INFO):
        #fmtName ='%(levelname)s %(asctime)s %(name)s: %(message)s'
        #formatter = logging.Formatter(fmt = fmtName, datefmt = '%m/%d/%Y %H:%M:%S') \
        #    if name else logging.Formatter(fmt = fmt, datefmt = '%m/%d/%Y %H:%M:%S')
        logger = logging.getLogger(name)
        logger.setLevel(level)

        stream_log = logging.StreamHandler()
        stream_log.setLevel(level)
        stream_log.setFormatter(cls.formatter)
        logger.addHandler(stream_log)

        log_file_path = cls.getLogFilePath(cls.file_name)
        log_file = open(log_file_path,'a')
        file_log = logging.FileHandler(log_file_path)
        file_log.setLevel(level)
        file_log.setFormatter(cls.formatter)
        logger.addHandler(file_log)

        return logger

    @classmethod
    def getLogFilePath(cls,name):
        return log_dir_path + name + date.today().strftime("_%y%m%d")

    @classmethod
    def getScrapyLoggerSetting(cls):
        return {
            'LOG_LEVEL': logging.DEBUG,
            'LOG_FORMAT': cls.fmt,
            'LOG_FILE': cls.getLogFilePath('scrapyroot')
        }

# import logging
# from twisted.python import log

# class LevelFileLogObserver(log.FileLogObserver):

#     def __init__(self, f, level=logging.INFO):
#         log.FileLogObserver.__init__(self, f)
#         self.logLevel = level

#     def emit(self, eventDict):
#         if eventDict['isError']:
#             level = logging.ERROR
#         elif 'level' in eventDict:
#             level = eventDict['level']
#         else:
#             level = logging.INFO
#         if level >= self.logLevel:
#             log.FileLogObserver.emit(self, eventDict)

# from twisted.python import logfile

# f = logfile.LogFile("someFile.log", '/some/path/', rotateLength=1000,
#                     maxRotatedFiles=100)
# logger = LevelFileLogObserver(f, logging.DEBUG)
# twisted.python.log.addObserver(logger.emit)