from fantasy.game_data.persistence.settings import *
from game_crawler.nba.nba.spiders.schedulesina import ScheduleSinaSpider
#from game_crawler.nba.nba.spiders.playerssina import PlayersSpiderSina
#from game_crawler.nba.nba.spiders.gamesina import GameSinaSpider
#from game_crawler.logger import CrawlerLogger 
from datetime import date

def main():
    kwargs = {'year':"2016", 'month':"10"}
    scheduleSinaSpider = ScheduleSinaSpider(**kwargs)
    scheduleSinaSpider.crawl()
    #ScheduleSinaSpider.run("2016","10")
    #PlayersSpiderSina.run()
    #kwargs = {'url':'http://nba.sports.sina.com.cn/look_scores.php?id=2016100228', 'date':date(2016,10,2)}
    #GameSinaSpider.run(**kwargs)

if __name__ == "__main__":
    main()