from datetime import datetime
import os

from twisted.internet import reactor
from apscheduler.schedulers.twisted import TwistedScheduler

from ..settings import *
from game_crawler.nba.nba.spiders.schedulesina import ScheduleSinaSpider
from game_crawler.nba.nba.spiders.playerssina import PlayersSpiderSina
from game_crawler.nba.nba.spiders.gamesina import GameSinaSpider
from game_crawler.logger import CrawlerLogger 
from datetime import date 

def tick():
    print('Tick! The time is: %s' % datetime.now())

def run_schedule_sina_spider():
    kwargs = {'year':"2016", 'month':"10"}
    ScheduleSinaSpider.run(**kwargs)
    #ScheduleSinaSpider.run("2016","10")
    #PlayersSpiderSina.run()

def run_game_sina_spider():    
    kwargs = {'url':'http://nba.sports.sina.com.cn/look_scores.php?id=2016100228', 'date':date(2016,10,2)}
    GameSinaSpider.run(**kwargs)

if __name__ == '__main__':
    scheduler = TwistedScheduler()
    #scheduler.add_job(run_schedule_sina_spider, 'interval', minutes=5)
    scheduler.add_job(run_game_sina_spider, 'interval', minutes=1)
    scheduler.start()
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

    # Execution will block here until Ctrl+C (Ctrl+Break on Windows) is pressed.
    try:
        reactor.run()
    except (KeyboardInterrupt, SystemExit):
        pass




