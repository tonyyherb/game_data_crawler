# -*- coding: utf-8 -*- 
from datetime import date 
from game_crawler.logger import CrawlerLogger
from game_crawler.nba.engine.crawlerbase import CrawlerBase
from parsel import Selector
import re

spider_name = "game_sina_crawler"
_logger = CrawlerLogger.getLogger(spider_name)

class GameSinaSpider(CrawlerBase):
    name = spider_name
    
    def __init__(self, start_url, date, *args, **kwargs):
        self.gameID = re.search(""".+\?id=(\d+)""", url).group(1)
        self.game_date = date
        _logger.info("Init spider for date %s, gameId %s at %s" % (self.game_date, self.gameID, self.start_urls))
        super(GameSinaSpider, self).__init__(start_url, *args, **kwargs)

    def parse(self, response):
        sel = Selector(response)
        headers = sel.xpath('//*[@id="left"]/table[2]/tr[2]/td/table/tr[1]/td[position()>1]/text()').extract()
        
        trs = (sel.xpath('//*[@id="left"]/table[2]/tr[2]/td/table/tr[position()>1]'))
        trs.extend(sel.xpath('//*[@id="left"]/table[3]/tr[2]/td/table/tr[position()>1]'))   
        clTheme = None
        for tr in trs:
            thisClTheme = tr.xpath('@bgcolor').extract()
            if not clTheme or thisClTheme == clTheme:
                clTheme = thisClTheme
                statItem = PlayerGameStatItem()
                code = tr.xpath('td[1]/a/@href').extract()
                name = tr.xpath('td[1]/a/text()').extract()
                mins = tr.xpath('td[2]/text()').extract()
                if mins and mins[0].strip().isdigit():
                    fg = tr.xpath('td[3]/text()').extract()
                    three = tr.xpath('td[4]/text()').extract()
                    ft = tr.xpath('td[5]/text()').extract()
                    oReb = tr.xpath('td[6]/text()').extract()
                    dReb = tr.xpath('td[7]/text()').extract()
                    tReb = tr.xpath('td[8]/text()').extract()
                    ast = tr.xpath('td[9]/text()').extract()
                    stl = tr.xpath('td[10]/text()').extract()
                    blk = tr.xpath('td[11]/text()').extract()
                    trno = tr.xpath('td[12]/text()').extract()
                    pts = tr.xpath('td[14]/text()').extract()
                    played = True
                    [fgm, fga] = fg[0].split("-") if fg else [None, None]
                    [ftm, fta] = ft[0].split("-") if ft else [None, None]
                    [threePtm, threePta] = three[0].split("-") if three else [None, None]
                    statItem['fgm'], statItem['fga'] = (fgm.strip(), fga.strip()) if fga else (0, 0)
                    statItem['ftm'], statItem['fta'] = (ftm.strip(), fta.strip()) if fta else (0, 0)
                    statItem['threePtm'], statItem['threePta'] = (threePtm.strip(), threePta.strip()) if threePta else (0, 0)
                    statItem['oReb'] = oReb[0].strip() if oReb else 0
                    statItem['tReb'] = tReb[0].strip() if tReb else 0
                    statItem['ast'] = ast[0].strip() if ast else 0
                    statItem['stl'] = stl[0].strip() if stl else 0
                    statItem['blk'] = blk[0].strip() if blk else 0
                    statItem['trno'] = trno[0].strip() if trno else 0
                    statItem['pts'] = pts[0].strip() if pts else 0
                    
                else:
                    played = False
                    statItem['fgm'], statItem['fga'] = 0, 0
                    statItem['ftm'], statItem['fta'] = 0, 0
                    statItem['threePtm'], statItem['threePta'] = 0, 0
                    statItem['oReb'] = 0
                    statItem['tReb'] = 0
                    statItem['ast'] = 0
                    statItem['stl'] = 0
                    statItem['blk'] = 0
                    statItem['trno'] = 0
                    statItem['pts'] = 0

                statItem['id'] = self.gameID
                statItem['gameDate'] = self.game_date
                playerId = code[0][code[0].index('=') + 1:] if code else None
                statItem['player_id'] = playerId
                statItem['playerNameCh'] = name[0].strip() if name else None
                statItem['played'] = played
                statItem['mins'] = mins[0].strip() if played else 0
                statItem['id'] = playerId + u"_" + self.game_date.strftime('%Y%m%d')
                yield statItem

    