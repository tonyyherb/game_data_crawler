# -*- coding: utf-8 -*- 
from game_crawler.nba.nba.item.sina import *
from game_crawler.spiderrunner import SpiderRunner
from scrapy import Spider
from scrapy.selector import Selector
from scrapy.utils.project import get_project_settings
import re
from game_crawler.logger import CrawlerLogger

spider_name = "players_crawler_sina"
_logger = CrawlerLogger.getLogger(spider_name)

class PlayersSpiderSina(SpiderRunner):
    """
    """
    
    name = spider_name
    allowed_domains = ["http://nba.sports.sina.com.cn/"]
#     start_urls = [("http://nba.sports.sina.com.cn/match_result.php?day=0&years=" + str(year) + "&months=%s&teams=" % str(x).zfill(2)) for x in range(1, 5)]
    start_urls = ["http://nba.sports.sina.com.cn/players.php?key=t"]

    def parse(self, response):
#         f = codecs.open("file2", "ab", "utf-8")
        sel = Selector(response)
        trs = sel.xpath('//*[@id="table980middle"]/div/table/tr')
        team, team_id = None, None
        playerNumRegex = re.compile(u'(\d+)\u53f7.*', re.U)
        for tr in trs:
            teamRaw = tr.xpath('td[@bgcolor]/a/text()').extract()
            if teamRaw:
                team = teamRaw
                team_id = tr.xpath('td[@bgcolor]/a/@href').extract()
            else:
                for td in tr.xpath('td'):
                    player_name = td.xpath('a/text()').extract()
                    if player_name:
                        player_number_raw = td.xpath('text()').extract()
                        player_number = playerNumRegex.match(player_number_raw[0] if player_number_raw else None)
                        player_id = td.xpath('a/@href').extract()
                        playerInfoItem = PlayerInfoItem()
                        playerInfoItem['id'] = player_id[0][player_id[0].index('=')+1:].strip() if player_id else None
                        [player_name_ch, player_name_en] = player_name[0].split(",") if player_name else [None, None]
                        playerInfoItem['playernameCh'], playerInfoItem['playernameEn'] = player_name_ch.strip() , player_name_en.strip()
                        playerInfoItem['teamNameCh'] = team[0].strip() if team else None
                        playerInfoItem['teamId'] = team_id[0][team_id[0].index('=')+1:].strip() if team_id else None
                        playerInfoItem['playerNumber'] = player_number.group(1) if player_number else None
                        yield playerInfoItem
    