# -*- coding: utf-8 -*- 
from scrapy import Item, Field 

class ScheduleItem(Item):
    gameDate = Field()
    startTime = Field()
    status = Field()
    gameType = Field()
    homeTeam = Field()
    awayTeam = Field()
    homeTeamCode = Field()
    awayTeamCode = Field()
    id = Field()

class PlayerInfoItem(Item):
    id = Field()

    playernameCh = Field()
    playernameEn = Field()
    teamId = Field()
    teamNameCh = Field()
    playerNumber = Field()

class PlayerGameStatItem(Item):
    id = Field()
    player_id = Field()
    gameDate = Field()

    playerNameCh = Field()
    mins = Field()
    fgm = Field()
    fga = Field()
    threePtm = Field()
    threePta = Field()
    ftm = Field()
    fta = Field()
    oReb = Field()
    tReb = Field()
    ast = Field()
    stl = Field()
    blk = Field()
    trno = Field()
    pts = Field()
    played = Field()
    lock = Field()
    fgp = Field()
#     three_ptp = models.FloatField(max_digits=5, decimal_places=4,null=True)
    ftp = Field()
    assistTrnoRatio = Field() 


    