# -*- coding: utf-8 -*- 
from game_crawler.nba.nba.item.sina import *
from game_crawler.spiderrunner import SpiderRunner
from scrapy import Spider
from scrapy.selector import Selector
import re
from game_crawler.logger import CrawlerLogger

from scrapy.utils.log import configure_logging

spider_name = "schedule_sina"
_logger = CrawlerLogger.getLogger(spider_name)

class ScheduleSinaSpider(SpiderRunner):
    configure_logging(CrawlerLogger.getScrapyLoggerSetting(),True)
    name = spider_name
    dateRegex = re.compile(u'^([0-9]{2})\u6708([0-9]{2})\u65e5.*', re.U)  # 01月01日
    gameReportRegex = re.compile('http://sports.sina.com.cn/nba/(.*)/.*')
    gameTimeRegex = re.compile(u'^(\d{2}:\d{2}) (.*)')

    allowed_domains = ["http://nba.sports.sina.com.cn/"]

    def __init__(self, year=None, month=None, *args, **kwargs):
        """
        Args:
            year (int): The first parameter.
            month (str): The second parameter.

        """
        # settings = get_project_settings().get('SPIDER_SCHEDULE_SINA')
        self.year = year #str(year if year else settings.get('year'))
        self.month = month #str (month if month else settings.get('month'))
        self.start_urls = ["http://nba.sports.sina.com.cn/match_result.php?day=0&years=%s&months=%s&teams=" % (self.year, self.month.zfill(2))]
        _logger.info("Init spider for year %s, month %s at %s" % (self.year, self.month, self.start_urls))
        super(ScheduleSinaSpider, self).__init__(self, *args, **kwargs)

    def parse(self, response):
        #_logger.info("Parsing...")
        sel = Selector(response)
        trs = sel.xpath('//*[@id="table980middle"]/div/table/tr')

        gameDate = None
        for tr in trs:
            gameDateTr = tr.xpath('td[1]/text()').extract()
            gameDateMatch = self.dateRegex.match(gameDateTr[0].strip()) if gameDateTr else None
            if gameDateMatch:
                gameDate = self.year + "-" + gameDateMatch.group(1) + "-" + gameDateMatch.group(2)

            startTimeRaw = tr.xpath('td[1]/text()').extract()
            gameType = tr.xpath('td[2]/text()').extract()
            awayCode = tr.xpath('td[3]/a/@href').extract()
            awayName = tr.xpath('td[3]/a/text()').extract()   
            homeCode = tr.xpath('td[5]/a/@href').extract()
            homeName = tr.xpath('td[5]/a/text()').extract()
#                 gameReport = tr.xpath('td[8]/a/@href').extract()
            gameStats = tr.xpath('td[9]/a/@href').extract()

            scheduleItem = ScheduleItem()
#                 gameReportMatch = gameReportRegex.match(gameReport[0].strip()) if gameReport else None
#                 gameDate = gameReportRegex.match(gameReport[0].strip()).group(1)
            startTimeMatch = self.gameTimeRegex.match(startTimeRaw[0].strip()) if startTimeRaw else None
            gameTime = startTimeMatch.group(1) if startTimeMatch else None
            gameStatus = startTimeMatch.group(2) if startTimeMatch else None
            
            scheduleItem['gameDate'] = gameDate if gameDate else None
            scheduleItem['startTime'] = gameTime if gameTime else None
            scheduleItem['status'] = gameStatus if gameStatus else None
            scheduleItem['gameType'] = gameType[0] if gameType else None
            scheduleItem['homeTeam'] = homeName[0] if homeName else None
            scheduleItem['homeTeamCode'] = int(homeCode[0][12:]) if homeCode else None
            scheduleItem['awayTeam'] = awayName[0] if awayName else None  # get player code digit
            scheduleItem['awayTeamCode'] = int(awayCode[0][12:]) if awayCode else None  # get player code digit                
            scheduleItem['id'] = gameStats[0][-10:] if gameStatus else None
            yield scheduleItem if (gameDate and gameTime) else None

    # @classmethod
    # def run(cls, year, month):
    #     """
    #     Args:
    #         year (str): year to crawl.
    #         month (str): month to crawl.
    #     """
    #     #self.year = year
    #     #self.month = month
    #     kwargs = {'year':year, 'month':month}
    #     super(ScheduleSinaSpider, cls).run(**kwargs)
        #_logger.info("Run %s for year %s, month %s at %s" % (self.name, self.year, self.month, self.start_urls))        
        #_logger.info("Run %s for year %s, month %s" % (spider_name, year, month))