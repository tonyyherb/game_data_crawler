from game_data.persistence.sina import ScheduleSina, PlayerSina, PlayerGameStatSina
from item.sina import ScheduleItem, PlayerInfoItem, PlayerGameStatItem
from game_data.persistence.database import DatabaseService

class GamePipeline(object):
    def open_spider(self, spider):
        self.db = DatabaseService()

    def close_spider(self, spider):
        self.db.commit()
        self.db.close()

    def process_item(self, item, spider):
        if isinstance(item, ScheduleItem):
            data = ScheduleSina.fromItem(item)
        elif isinstance(item, PlayerInfoItem):
            data = PlayerSina.fromItem(item)
        elif isinstance(item, PlayerGameStatItem):
            data = PlayerGameStatSina.fromItem(item)
        else:
            raise Exception('Unknow Item Class')

        self.db.save_or_update(data)
        return item