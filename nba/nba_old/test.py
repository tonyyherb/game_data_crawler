from importlib import import_module
import sys
import os
CRAWLER_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__))) # game_crawler
BASE_DIR = os.path.dirname(CRAWLER_DIR) # fantasy

sys.path.append(BASE_DIR)
sys.path.append(CRAWLER_DIR)
print(sys.path)
mod = import_module("game_crawler.nba.nba.pipelines")
obj = getattr(mod, "GamePipeline")
