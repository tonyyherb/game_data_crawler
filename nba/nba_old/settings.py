# -*- coding: utf-8 -*-
import os
import sys

# CRAWLER_DIR_ABS = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# SETTINGS_DIR_ABS = os.path.dirname(os.path.abspath(__file__))
# BASE_DIR_ABS = os.path.dirname(CRAWLER_DIR) # fantasy

CRAWLER_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__))) # game_crawler
BASE_DIR = os.path.dirname(CRAWLER_DIR) # fantasy

sys.path.append(BASE_DIR)
sys.path.append(CRAWLER_DIR)
# sys.path.append(CRAWLER_DIR_ABS)
# sys.path.append(SETTINGS_DIR_ABS)

# print(sys.path)

# Scrapy settings for tutorial project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'nba'

SPIDER_MODULES = ['game_crawler.nba.nba.spiders']
NEWSPIDER_MODULE = 'game_crawler.nba.nba.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'tutorial (+http://www.yourdomain.com)'

# Configure maximum concurrent requests performed by Scrapy (default: 16)

ITEM_PIPELINES = {
    'game_crawler.nba.nba.pipelines.GamePipeline': 999,
}

SPIDER_GAME_SINA = {
    'http_prefix': """http://nba.sports.sina.com.cn/look_scores.php?id=""",
} 