from urllib import urlopen

class CrawlerBase(object):
    name = ""
    def __init__(self, start_url, *args, **kwargs):
        self.start_url = start_url

    def crawl(self):
        response = self.make_requests_from_url()
        items = self.parse(response)
        self.persist(items)

    def parse(self):
        raise NotImplementedError

    def persist(self):
        raise NotImplementedError

    def make_requests_from_url(self):
        response = urlopen(self.start_url)
        html = response.read().decode("utf-8")
        return html
