# -*- coding: utf-8 -*- 
from datetime import date, datetime, timedelta
from game_data.persistence.sina.schedule import ScheduleSina
from game_data.query.sinaquery import QuerySina
from game_crawler.nba.nba.settings import SPIDER_GAME_SINA

_logger = CrawlerLogger.getLogger("crawler_engine")

class EngineSina(object):
    prefix = SPIDER_GAME_SINA.get('prefix')

    def start(crawl_date = None):
    """
    Crawl this schedule; crawl finished uncrawled games' stat
    If next game already started =>  run periodic_crawl and schedule at interval
    else => schedule periodic_crawl task at next crawl time

    Args:
        crawl_date(date): date to start crawling
    """ 
        crawl_date = crawl_date if crawl_date else date.today()
        _logger.info("CrawlerEngine started for %s" % crawl_date)
        ScheduleSinaCrawler.run(str(crawl_date.year),str(crawl_date.month))
        crawl_finished_games_stat()

    def crawl_finished_games_stat():
    """Query db for games that crawled==False and then calls :func:`crawl_games`"""
    
        _logger.info("Crawling stats for finished games")
        uncrawled_finished_games = QuerySina().getUncrawledFinishedGames()
        id_date_set = set((schedule.id, schedule.gameDate) for schedule in uncrawled_finished_games)
        game_url_date_pairs = [(prefix + ele[0], ele[1]) for schedule in id_date_set]
        crawl_games(game_url_date_pairs)

        connection.close()
        crawled_games = Schedule.objects.filter(game_id__in=uncrawled_finished_game)
        for game in crawled_games:
            game.crawled = True
            game.save()
        logger.info("crawled stats for %s games" % len(uncrawled_finished_game))     
    
    def crawl_games(game_url_date_pairs):
        for pair in game_url_date_pairs:
            spider = NbaGameSpiderSina(url=pair[0], date=pair[1])
            crawler = startCrawlerScript(spider)
            crawler.start()
            crawler.join()        