# -*- coding: utf-8 -*- 
from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy import Spider
from scrapy.utils.project import get_project_settings
from scrapy.utils.log import configure_logging
from game_crawler.logger import CrawlerLogger
import os

configure_logging(CrawlerLogger.getScrapyLoggerSetting(),True)
os.environ['SCRAPY_SETTINGS_MODULE'] = 'game_crawler.nba.nba.settings'

class SpiderRunner(Spider):
    def __init__(self, *args, **kwargs):
        super(SpiderRunner, self).__init__(*args, **kwargs)

    @classmethod
    def run(cls, **kwargs):
        settings = get_project_settings()
        runner = CrawlerRunner(settings)
        #print("SpiderRunner class:%s" % cls)
        d = runner.crawl(cls, **kwargs) if kwargs else runner.crawl(cls)
        d.addBoth(lambda _: reactor.stop())
        reactor.run()