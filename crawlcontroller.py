from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from scrapy.utils.log import configure_logging
from game_crawler.logger import CrawlerLogger
import os

#configure_logging(CrawlerLogger.getScrapyLoggerSetting())
#os.environ['SCRAPY_SETTINGS_MODULE'] = 'game_crawler.nba.nba.settings'

class CrawlController(object):
    def __init__(self, spider):
        self.spider = spider

    def run(self, kwargs=None):
        settings = get_project_settings()
        runner = CrawlerRunner(settings)
        #runner = CrawlerRunner()
        print(settings['ITEM_PIPELINES'].attributes)
        d = runner.crawl(self.spider, **kwargs) if kwargs else runner.crawl(self.spider)
        d.addBoth(lambda _: reactor.stop())
        reactor.run()